# OpenML dataset: UEFA-Champions-league-Player-Statistics

https://www.openml.org/d/43510

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset has an exhaustive list of player statistics for each season from 2013 - 2020
Content
Each row is associated with a player and a season.
Eg. You will have 7 rows for Lionel Messi: 1 for each season he played
Each row will have 103 unique stats you can look at (Eg. No of Goals Scored, Passing accuracy in , Minutes Played etc)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43510) of an [OpenML dataset](https://www.openml.org/d/43510). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43510/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43510/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43510/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

